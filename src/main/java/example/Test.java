package example;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {

    public static void main(String[] args) {

        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();


//        Student s1 = new Student("Fitor", "Ceka", 5.7f);
//
//        session.persist(s1); // per te ruajtur te dhena ne databaze


//        Student found = session.find(Student.class, 1); // per te kerkuar
//        System.out.println(found);
//
//        found.setGrade(10f);
//        session.merge(found); // per te bere update te dhenash
//        found = session.find(Student.class, 1);
//        System.out.println(found);
//
//        session.remove(found); // per fshire te dhena
//        found = session.find(Student.class, 1);
//        System.out.println(found);

        CourseId c1 = new CourseId(1, "C#");
        Course c = new Course(c1, "This is a Java Course");

        session.persist(c);

        transaction.commit();
    }
}
