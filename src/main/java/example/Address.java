package example;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

@Embeddable
public class Address {

    @Column(name = "AP_NO")
    private int apartmentNumber;

    @Column(name = "STREET")
    private String street;

    @Column(name = "CITY")
    private String city;
}
