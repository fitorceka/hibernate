package example;

import jakarta.persistence.Embeddable;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CourseId implements Serializable {

    private int fId;
    private String name;

    public CourseId(int fId, String name) {
        this.fId = fId;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CourseId courseId = (CourseId) o;

        if (fId != courseId.fId) return false;
        return Objects.equals(name, courseId.name);
    }

    @Override
    public int hashCode() {
        int result = fId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
