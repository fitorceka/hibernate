package example;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "COURSE_INFO")
public class Course {

    @EmbeddedId
    private CourseId courseId;

    @Column(name = "DESCRIPTION")
    private String description;


    public Course() {
    }

    public Course(CourseId courseId, String description) {
        this.courseId = courseId;
        this.description = description;
    }
}
