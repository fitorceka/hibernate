package queries;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import relations.manytomany.Student;

import java.util.List;

public class Test {
    public static void main(String[] args) {
        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();

        String findAll = "From Student";

        Query<Student> query = session.createQuery(findAll, Student.class);

        List<Student> allStudents = query.getResultList();
//        allStudents.forEach(System.out::println);

        // find students that have 3 or more exams
        String f = "select S from Student S join S.exams E " +
                " group by S having count(E.examId) >= :countS";
        Query<Student> q = session.createQuery(f, Student.class);
        q.setParameter("countS", 3);
        List<Student> students = q.getResultList();
        students.forEach(System.out::println);

        transaction.commit();
    }
}
