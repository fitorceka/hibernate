package relations.onetoone;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;

import java.time.LocalDate;

@Entity
public class UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int udId;

    private LocalDate birthday;
    private String birthPlace;

    @OneToOne // to create a one to one relation
    @JoinColumn(name = "U_ID")
    private Users users;

    public UserDetails() {
    }

    public UserDetails(LocalDate birthday, String birthPlace, Users users) {
        this.birthday = birthday;
        this.birthPlace = birthPlace;
        this.users = users;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
}
