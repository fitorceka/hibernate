package relations.onetoone;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.time.LocalDate;

public class Test {

    public static void main(String[] args) {
        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();

        Users u1 = new Users("Fitor", "Ceka");
        UserDetails ud1 = new UserDetails(LocalDate.now(), "Klos", u1);

        session.persist(u1);
        session.persist(ud1);

        transaction.commit();
    }
}
