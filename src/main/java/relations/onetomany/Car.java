package relations.onetomany;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int carId;

    private String model;
    private int year;

    @ManyToOne
    @JoinColumn(name = "PERSON_ID")
    private Person person;

    public Car() {
    }

    public Car(String model, int year, Person person) {
        this.model = model;
        this.year = year;
        this.person  = person;
    }
}
