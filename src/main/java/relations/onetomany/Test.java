package relations.onetomany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {

    public static void main(String[] args) {
        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();


        Person p = new Person("Fitor Ceka", 27);
        Person p1 = new Person("Jurge Hoxha", 24);

        Car c1 = new Car("Benz", 2020, p);
        Car c2 = new Car("BMW", 2023, p);
        Car c3 = new Car("AUDI", 2023, p1);
        Car c4 = new Car("Fiat", 2023, p);

        session.persist(p);
        session.persist(c1);
        session.persist(c2);
        session.persist(p1);
        session.persist(c3);
        session.persist(c4);


        transaction.commit();
    }
}
