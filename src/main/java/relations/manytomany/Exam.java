package relations.manytomany;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;

import java.util.List;

@Entity
public class Exam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int examId;

    private String subject;

    @ManyToMany
    @JoinTable(name = "EXAM_STUDENT",
          joinColumns = @JoinColumn(name = "EXAM_ID"),
          inverseJoinColumns = @JoinColumn(name = "STUDENT_ID"))
    private List<Student> students;

    public void setExamId(int examId) {
        this.examId = examId;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Exam{" +
                "examId=" + examId +
                ", subject='" + subject + '\'' +
                '}';
    }
}
