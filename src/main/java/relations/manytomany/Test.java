package relations.manytomany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Test {
    public static void main(String[] args) {
        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();

        Student s1 = new Student();
        s1.setName("Fitor");
        Student s2 = new Student();
        s2.setName("Jurgen");
        Student s3 = new Student();
        s3.setName("Erion");

        Exam e1 = new Exam();
        e1.setSubject("Java");
        Exam e2 = new Exam();
        e2.setSubject("Math");
        Exam e3 = new Exam();
        e3.setSubject("English");

        e1.setStudents(List.of(s1, s2, s3));
        e2.setStudents(List.of(s1));
        e3.setStudents(List.of(s2, s3));


        session.persist(s1);
        session.persist(s2);
        session.persist(s3);
        session.persist(e1);
        session.persist(e2);
        session.persist(e3);

        transaction.commit();

    }
}
